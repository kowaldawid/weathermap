﻿var json = (function () {

    var _createJSON = function (ob) {
        console.log(JSON.stringify(ob));
        return JSON.stringify(ob, null, 2);
    }

    var _getJSON = function (url, callback) {
        jQuery.getJSON(url, function (data) {
            callback(data);
        });
    }

    var _getJSONP = function (url, infowindow, callback) {
        jQuery.ajax({
            url: url,
            dataType: "jsonp",
            jsonpCallback: "jsonpcallback",
            success: function (data) {
                callback(data, infowindow);
            },
            error: function (exception) {
                console.log("Error connect");
            }
        });
    }

    return {
        createJSON: _createJSON,
        getJSON: _getJSON,
        getJSONP: _getJSONP
    }

})();
    