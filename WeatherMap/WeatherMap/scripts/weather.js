﻿var weather = (function () {

    function add(data) {
        jQuery.each(data, function (k, value) {
            maps.addMarker(value.coord.lat, value.coord.lon, value.name, "id=", value.id);
        });
    }

    var _cityList = function () {
        json.getJSON("json/city.list.json", add);
    }   

    var i = 0;

    function viewInformation(data, infowindow) {
        var temperature, pressure, humidity, cloudy, windSpeed, description;
        console.log(data);

        temperature = data.main.temp;
        pressure = data.main.pressure;
        humidity = data.main.humidity;
        cloudy = data.clouds.all;
        windSpeed = data.wind.speed;
        description = data.weather[0].description;

        var viewDescription = '' +
        '<div>' +
            '<h3>' + (description ? description : 'brak danych') + '</h3>' +
            '<ul>' +
                '<li><strong>temperatura</strong>: ' + (temperature ? temperature + '°C' : 'brak danych') + '</li>' +
                '<li><strong>ciśnienie</strong>: ' + (pressure ? pressure + 'hPa' : 'brak danych') + '</li>' +
                '<li><strong>wilgotność</strong>: ' + (humidity ? humidity + '%' : 'brak danych') + '</li>' +
                '<li><strong>zachmurzenie</strong>: ' + (cloudy ? cloudy + '%' : 'brak danych') + '</li>' +
                '<li><strong>prędkośc wiatru</strong>: ' + (windSpeed ? windSpeed + 'm/s' : 'brak danych') + '</li>' +
            '</ul>' +
        '</div>';

        infowindow.setContent(viewDescription);
    }

    var _weatherInformation = function (id, infowindow) {
        var url = "http://api.openweathermap.org/data/2.5/weather?";
        url += id;
        url += "&lang=pl&units=metric&callback=jsonpcallback&appid=ca8bfb1a52c734beb4bd8e56400ffc15";
        console.log(url);
        json.getJSONP(url, infowindow, viewInformation);
    }

    return {
        cityList: _cityList,
        weatherInformation : _weatherInformation
    }

})();
