﻿'use strict';

var maps = (function () {

    var map;
    var marker;
    var location;

    var maps = {
        location: true,
        uluru: { lat: 51.919438, lng: 19.145136 }
    };

    var _initMap = function _initMap() {

        var styledMapType = styleMap();
        var elems = document.getElementById('map');
        map = new google.maps.Map(elems, {
            zoom: 6,
            scrollwheel: false,
            streetViewControl: false,
            fullscreenControl: false,
            center: maps.uluru,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                mapTypeIds: ['roadmap', 'styled_map']
            }
        });

        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
            }
            map.setZoom(8);
            _addMarker(place.geometry.location.lat(), place.geometry.location.lng(), place.name, "q=", place.name, true);
        });

        shareLocation();
    };

    function locationAccuracyPlaceCircleAndPanTo(latLng, radius) {
        location = new google.maps.Circle({
            strokeWeight: 0,
            fillColor: '#005dab',
            fillOpacity: 0.35,
            center: latLng,
            radius: radius,
            map: map,
            editable: false
        });
    }

    var _setPosition = function _setPosition(latitude, longitude) {
        maps.uluru = { lat: latitude, lng: longitude };
    };

    var _setLocation = function _setLocation(location) {
        maps.location = location;
    };

    function shareLocation() {
        if (navigator.geolocation && maps.location) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                locationAccuracyPlaceCircleAndPanTo(pos, position.coords.accuracy);
                map.fitBounds(location.getBounds());
                map.setCenter(pos);
            });
        } else if (maps.location) alert("Twoja przeglądarka nie wspiera geolokalizacji.");
    }

    var _addMarker = function _addMarker(latitude, longitude, text, where, id, openInfoWindow) {
        var infowindow = new google.maps.InfoWindow();
        var myLatLng = new google.maps.LatLng(latitude, longitude);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            animation: google.maps.Animation.DROP,
            label: {
                text: text,
                color: "#000",
                fontSize: "16px",
                fontWeight: "bold"
            }
        });

        var url = where + id;

        google.maps.event.addListener(marker, 'click', (function (marker, url) {
            return function () {
                //infowindow.setContent(weather.weatherInformation(id));
                infowindow.open(map, marker);
                weather.weatherInformation(url, infowindow);
            };
        })(marker, url));

        if (openInfoWindow = typeof openInfoWindow !== 'undefined' ? openInfoWindow : false) {
            infowindow.open(map, marker);
            weather.weatherInformation(url, infowindow);
        }

        map.panTo(myLatLng);
    };

    function styleMap() {
        return styledMapType = new google.maps.StyledMapType([{
            "featureType": "administrative",
            "elementType": "labels",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [{
                "color": "#f7f1df"
            }]
        }, {
            "featureType": "landscape.natural.terrain",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi.business",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi.medical",
            "elementType": "geometry",
            "stylers": [{
                "color": "#fbd3da"
            }]
        }, {
            "featureType": "road",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#ffe15f"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "black"
            }]
        }, {
            "featureType": "transit",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "transit.station.airport",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#cfb2db"
            }]
        }, {
            "featureType": "water",
            "stylers": [{
                "weight": 2
            }]
        }], { name: 'Another' });
    }

    return {
        init: _initMap,
        setPosition: _setPosition,
        setLocation: _setLocation,
        addMarker: _addMarker
    };
})();

