﻿"use strict";

var json = (function () {

    var _createJSON = function _createJSON(ob) {
        console.log(JSON.stringify(ob));
        return JSON.stringify(ob, null, 2);
    };

    var _getJSON = function _getJSON(url, callback) {
        jQuery.getJSON(url, function (data) {
            callback(data);
        });
    };

    var _getJSONP = function _getJSONP(url, infowindow, callback) {
        jQuery.ajax({
            url: url,
            dataType: "jsonp",
            jsonpCallback: "jsonpcallback",
            success: function success(data) {
                callback(data, infowindow);
            },
            error: function error(exception) {
                console.log("Error connect");
            }
        });
    };

    return {
        createJSON: _createJSON,
        getJSON: _getJSON,
        getJSONP: _getJSONP
    };
})();

